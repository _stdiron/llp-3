#include <xmlrpc.h>
#include <xmlrpc_client.h>
#include <stdio.h>
#include "../common/ast_xml_convert.h"
#include "parser.tab.h"
#include "client_util.h"

#define INPUT_BUFFER 512

int read_next(FILE* stream, char* buffer) {
    printf("\n");
    memset(buffer, 0, INPUT_BUFFER);
    if (stream == stdin)
        printf("> ");
    if (fscanf(stream, "%512[^;]c", buffer) <= 0)
        return 0;
    while (fgetc(stream) != ';') ;
    if (buffer[0] == '\0' || buffer[1] == '\0' || buffer[0] == EOF)
        return 0;
    printf("\n");
    return 1;
}

int main(int args_n, char* args[]) {
    xmlrpc_env env;
    if (args_n < 2) {
        fprintf(stderr, "You need to pass <server_address:port>\n");
        exit(1);
    }

    char* url = malloc(128);
    sprintf(url, "http://%s/RPC2", args[1]);
    char* buffer = malloc(INPUT_BUFFER);
    xmlrpc_env_init(&env);
    xmlrpc_client_init2(&env, XMLRPC_CLIENT_NO_FLAGS, "aql_parser_client",
                        "1.0", NULL, 0);

//    FILE* file = fopen("/home/vlad/Music/answers_new.aql", "r");
    while (read_next(stdin, buffer)) {
        Ast* ast = parse_ast(buffer);
        if (!ast) {
            continue;
        }
        xmlrpc_value* xml_ast = build_xmlrpc_from_ast(ast, &env);
        free_ast(ast);

        printf("fetching...\n");
        xmlrpc_value* response = xmlrpc_client_call(&env, url, "interpret_ast",
                                                    "(S)", xml_ast);
        xmlrpc_DECREF(xml_ast);
        xmlrpc_check_fault(&env);

        print_response(&env, response, stdout);

        if (xmlrpc_array_size(&env, response) == 6) {
            int index;
            xmlrpc_value* index_xml;
            xmlrpc_array_read_item(&env, response, 5, &index_xml);
            xmlrpc_read_int(&env, index_xml, &index);
            xmlrpc_DECREF(index_xml);
            while(index >= 0) {
                printf("Bunch of rows were fetched, but there are more on the server. Load next? (y/n)\n");
                char c;
                scanf(" %1c", &c);
                if (c == 'y')
                    printf("fetching...\n");
                else {
                    response = xmlrpc_client_call(&env, url, "destroy_operation",
                                                  "(i)", index);
                    xmlrpc_DECREF(response);
                    break;
                }
                xmlrpc_DECREF(response);
                response = xmlrpc_client_call(&env, url, "continue_operation",
                                              "(i)", index);
                xmlrpc_check_fault(&env);
                print_response(&env, response, stdout);
                if (xmlrpc_array_size(&env, response) != 6)
                    break;
                xmlrpc_array_read_item(&env, response, 5, &index_xml);
                xmlrpc_read_int(&env, index_xml, &index);
                xmlrpc_DECREF(index_xml);
            }
        }
        xmlrpc_DECREF(response);
    }
//    fclose(file);

    xmlrpc_env_clean(&env);
    free(url);
    free(buffer);
}
//
// Created by vlad on 10.01.24.
//

#include "client_util.h"

#define DATATYPE_INT 1
#define DATATYPE_FLOAT 2
#define DATATYPE_BOOL 3
#define DATATYPE_STRING 4

enum ResponseType { RESPONSE_ERROR, RESPONSE_DATA, RESPONSE_OK };
enum Intent { SI_NOTHING, SI_REMOVE, SI_GET, SI_UPDATE, SI_INSERT, SI_CREATE };

static char* intent_to_string[] = {
        [SI_NOTHING] = "nothing to be done",
        [SI_REMOVE] = "elements removed",
        [SI_GET] = "elements got",
        [SI_UPDATE] = "elements updated",
        [SI_INSERT] = "record inserted",
        [SI_CREATE] = "table created"
};

static char* datatype_to_string[] = {
        [DATATYPE_INT] = "integer",
        [DATATYPE_FLOAT] = "float",
        [DATATYPE_BOOL] = "boolean",
        [DATATYPE_STRING] = "string"
};

static TableScheme parse_scheme_xml(xmlrpc_env* env, xmlrpc_value* scheme_xml) {
    int scheme_sz = xmlrpc_array_size(env, scheme_xml);
    TableScheme scheme = create_table_scheme(scheme_sz);
    for (int i = 0; i < scheme_sz; i++) {
        char* name;
        TableDatatype type;
        xmlrpc_value* item;
        xmlrpc_array_read_item(env, scheme_xml, i, &item);
        xmlrpc_check_fault(env);
        xmlrpc_decompose_value(env, item, "(si)", &name, &type);
        xmlrpc_check_fault(env);
        add_scheme_field(&scheme, name, type, 1);
        xmlrpc_DECREF(item);
        free(name);
    }
    return scheme;
}

void print_scheme(TableScheme* scheme, FILE* stream) {
    fprintf(stream, "columns: ");
    for (SchemeItem* it = scheme->fields; it < scheme->fields+scheme->size; it++) {
        fprintf(stream, "%10.10s ", it->name);
    }
    fprintf(stream, "\ntypes:   ");
    for (SchemeItem* it = scheme->fields; it < scheme->fields+scheme->size; it++) {
        fprintf(stream, "%10.10s ", datatype_to_string[it->type]);
    }
    fprintf(stream, "\n");
}

static void print_row_xml(xmlrpc_env* env, xmlrpc_value* row_xml, TableScheme* scheme, FILE* stream) {
    fprintf(stream, "{\n");
    for (int i = 0; i < scheme->size; i++) {
        xmlrpc_value* value;
        xmlrpc_array_read_item(env, row_xml, i, &value);
        xmlrpc_check_fault(env);
        switch (scheme->fields[i].type) {
            case DATATYPE_BOOL: {
                int got_int;
                xmlrpc_read_int(env, value, &got_int);
                xmlrpc_check_fault(env);
                fprintf(stream, "    %s: %d\n", scheme->fields[i].name, got_int);
                break;
            }
            case DATATYPE_INT: {
                int got_int;
                xmlrpc_read_int(env, value, &got_int);
                xmlrpc_check_fault(env);
                fprintf(stream, "    %s: %d\n", scheme->fields[i].name, got_int);
                break;
            }
            case DATATYPE_FLOAT: {
                double got_float;
                xmlrpc_read_double(env, value, &got_float);
                xmlrpc_check_fault(env);
                fprintf(stream, "    %s: %f\n", scheme->fields[i].name, (float)got_float);
                break;
            }
            case DATATYPE_STRING: {
                const char* got_str;
                xmlrpc_read_string(env, value, &got_str);
                xmlrpc_check_fault(env);
                fprintf(stream, "    %s: %s\n", scheme->fields[i].name, got_str);
                free((void*)got_str);
                break;
            }
            default:
                fprintf(stderr, "ERROR INCOMPATIBLE DATATYPE");
                exit(1);
        }
        xmlrpc_DECREF(value);
    }
    fprintf(stream, "}\n");
}


void print_xml(xmlrpc_env* env, xmlrpc_value* xml_data) {
    xmlrpc_mem_block* memblock = XMLRPC_MEMBLOCK_NEW(char, env, 0);
    xmlrpc_value* params = xmlrpc_build_value(env, "(S)", xml_data);
    xmlrpc_serialize_call(env, memblock, "my_method_name", params);
    xmlrpc_check_fault(env);

    fwrite(XMLRPC_MEMBLOCK_CONTENTS(char, memblock),
    sizeof(char),
            XMLRPC_MEMBLOCK_SIZE(char, memblock),
    stdout);

    XMLRPC_MEMBLOCK_FREE(char, memblock);
}

void print_response(xmlrpc_env* env, xmlrpc_value* response, FILE* stream) {
    xmlrpc_value* type_xml;
    int type;
    xmlrpc_array_read_item(env, response, 0, &type_xml);
    xmlrpc_read_int(env, type_xml, &type);
    xmlrpc_DECREF(type_xml);
    xmlrpc_check_fault(env);
    if (type == RESPONSE_ERROR) {
        xmlrpc_value* message_xml;
        const char* message;
        xmlrpc_array_read_item(env, response, 1, &message_xml);
        xmlrpc_read_string(env, message_xml, &message);
        xmlrpc_check_fault(env);
        fprintf(stream, "ERROR: %s.\n", message);
        return;
    }

    xmlrpc_value* si_type_xml;
    xmlrpc_value* scheme_xml;
    int si_type;
    xmlrpc_array_read_item(env, response, 1, &si_type_xml);
    xmlrpc_read_int(env, si_type_xml, &si_type);
    xmlrpc_DECREF(si_type_xml);
    if (si_type == SI_NOTHING) {
        fprintf(stream, "Success: %s.\n", intent_to_string[si_type]);
        return;
    }
    xmlrpc_array_read_item(env, response, 2, &scheme_xml);
    xmlrpc_check_fault(env);
    TableScheme scheme = parse_scheme_xml(env, scheme_xml);
    xmlrpc_DECREF(scheme_xml);

    if (si_type == SI_INSERT || si_type == SI_CREATE) {
        fprintf(stream, "Success: %s.\n", intent_to_string[si_type]);
        print_scheme(&scheme, stream);
        free_scheme(scheme.fields, scheme.size);
        return;
    }

    xmlrpc_value* el_affected_xml;
    int el_affected;
    xmlrpc_array_read_item(env, response, 3, &el_affected_xml);
    xmlrpc_read_int(env, el_affected_xml, &el_affected);
    xmlrpc_DECREF(el_affected_xml);

    fprintf(stream, "Success: %d %s.\n", el_affected, intent_to_string[si_type]);
    print_scheme(&scheme, stream);
    if (type == RESPONSE_OK)
        return;

    assert(type == RESPONSE_DATA);
    xmlrpc_value* rows_xml;
    xmlrpc_array_read_item(env, response, 4, &rows_xml);
    assert(el_affected == xmlrpc_array_size(env, rows_xml));
    for (int i = 0; i < el_affected; i++) {
        xmlrpc_value* row_xml;
        xmlrpc_array_read_item(env, rows_xml, i, &row_xml);
        print_row_xml(env, row_xml, &scheme, stream);
        xmlrpc_DECREF(row_xml);
    }
    free_scheme(scheme.fields, scheme.size);
    xmlrpc_DECREF(rows_xml);
}
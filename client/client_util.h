//
// Created by vlad on 10.01.24.
//

#ifndef LAB3_CLIENT_UTIL_H
#define LAB3_CLIENT_UTIL_H

#include <xmlrpc.h>
#include <stdio.h>
#include <assert.h>
#include "../common/ast_xml_convert.h"
#include "../server/database/table.h"

void print_response(xmlrpc_env* env, xmlrpc_value* response, FILE* stream);

#endif //LAB3_CLIENT_UTIL_H

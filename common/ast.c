#include <stdio.h>
#include "ast.h"

static char* CONDITIONS[] = {
        [NT_COND_EQ - NT_COND_EQ] = "==",
        [NT_COND_NEQ - NT_COND_EQ] = "!=",
        [NT_COND_GR - NT_COND_EQ] = ">",
        [NT_COND_LE - NT_COND_EQ] = "<",
        [NT_COND_EGR - NT_COND_EQ] = ">=",
        [NT_COND_ELE - NT_COND_EQ] = "<=",
        [NT_COND_SUBSTR - NT_COND_EQ] = "in",
        [NT_COND_AND - NT_COND_EQ] = "AND",
        [NT_COND_OR - NT_COND_EQ] = "OR",
};

static char* VARTYPES[] = { "int", "float", "string", "bool"};

void add_at_end(struct Ast* root, struct Ast* node) {
    struct Ast* c;
    for (c = root; c->r; c = c->r) ;
    c->r = node;
}

struct Ast* create_node(int type, struct Ast* l, struct Ast* r) {
    struct Ast* ast = malloc(sizeof(struct Ast));
    ast->type = type;
    ast->l = l;
    ast->r = r;
    return ast;
}


struct Ast* create_int_node(int val) {
    struct NInt* ast = malloc(sizeof(struct NInt));
    ast->type = NT_INT;
    ast->val = val;
    return (struct Ast*)ast;
}

struct Ast* create_float_node(float val) {
    struct NFloat* ast = malloc(sizeof(struct NFloat));
    ast->type = NT_FLOAT;
    ast->val = val;
    return (struct Ast*)ast;
}

struct Ast* create_string_node(char* val, int type) {
    struct NString* ast = malloc(sizeof(struct NString));
    ast->type = type;
    ast->val = val;
    ast->r = 0;
    return (struct Ast*)ast;
}

struct Ast* create_bool_node(int val) {
    struct NInt* ast = malloc(sizeof(struct NInt));
    ast->type = NT_BOOL;
    ast->val = val;
    return (struct Ast*)ast;
}

struct Ast* create_string_child_node(int type, char* s, struct Ast* l) {
    struct NStringWChild* ast = malloc(sizeof(struct NStringWChild));
    ast->type = type;
    ast->s = s;
    ast->l = l;
    ast->r = NULL;
    return (struct Ast*)ast;
}

struct Ast* create_dstring_child_node(int type, char* s1, char* s2, struct Ast* l) {
    struct NDStringWChild* ast = malloc(sizeof(struct NDStringWChild));
    ast->type = type;
    ast->l = l;
    ast->r = NULL;
    ast->s1 = s1;
    ast->s2 = s2;
    return (struct Ast*)ast;
}

struct Ast* create_dstring_node(int type, char* s1, char* s2) {
    struct NDoubleString* ast = malloc(sizeof(struct NDoubleString));
    ast->type = type;
    ast->r = NULL;
    ast->s1 = s1;
    ast->s2 = s2;
    return (struct Ast*)ast;
}

char* get_condition_name(int val) {
    return CONDITIONS[val-NT_COND_EQ];
}

char* get_vartype_name(int val) {
    return VARTYPES[val-NT_DEF_INT];
}

void print_indent(FILE* stream, int depth) {
    char* indent = malloc(depth*2+1);
    memset(indent, ' ', depth*2+1);
    indent[depth*2] = '\0';
    fprintf(stream, "\n");
    fprintf(stream, "%s", indent);
    free(indent);
}

void free_ast(struct Ast* node) {
    if (node == NULL)
        return;
    switch (node->type) {
        case NT_INSERT:
        case NT_CREATE:
        case NT_PAIR: {
            struct NStringWChild* n = (struct NStringWChild*)node;
            free_ast(n->l);
            free_ast(n->r);
            if (n->s)
                free(n->s);
            break;
        }
        case NT_UPDATE:
        case NT_FOR: {
            struct NDStringWChild* n = (struct NDStringWChild*)node;
            free_ast(n->l);
            free_ast(n->r);
            if (n->s1)
                free(n->s1);
            if (n->s2)
                free(n->s2);
            break;
        }
        case NT_MERGE:
        case NT_FILTER:
        case NT_RET:
        case NT_COND_EQ:
        case NT_COND_NEQ:
        case NT_COND_GR:
        case NT_COND_LE:
        case NT_COND_EGR:
        case NT_COND_ELE:
        case NT_COND_SUBSTR:
        case NT_COND_AND:
        case NT_COND_OR:
            free_ast(node->l);
            free_ast(node->r);
            break;
        case NT_REMOVE:
        case NT_POINT_OP: {
            struct NDoubleString* n = (struct NDoubleString*)node;
            free_ast(n->r);
            if (n->s1)
                free(n->s1);
            if (n->s2)
                free(n->s2);
            break;
        }
        case NT_STR:
        case NT_DEF_BOOL:
        case NT_DEF_FLOAT:
        case NT_DEF_INT:
        case NT_DEF_STR:
        case NT_VAR: {
            struct NString* n = (struct NString*)node;
            free_ast(n->r);
            if (((struct NString*)node)->val)
                free(((struct NString*)node)->val);
            break;
        }
        case NT_FLOAT:
        case NT_INT:
        case NT_BOOL:
            break;
    }
    free(node);
}

void print_ast(FILE* stream, struct Ast* node, int depth) {
    if (node == NULL)
        return;
    print_indent(stream, depth);
    switch (node->type) {
        case NT_REMOVE: {
            struct NDoubleString* n = (struct NDoubleString*)node;
            fprintf(stream, "remove: {");
            print_indent(stream, depth+1);
            fprintf(stream, "table_alias: %s", n->s1);
            print_indent(stream, depth+1);
            fprintf(stream, "table: %s", n->s2);
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, n->r, depth);
            break;
        }
        case NT_INSERT: {
            struct NStringWChild* n = (struct NStringWChild*)node;
            fprintf(stream, "insert : {");
            print_indent(stream, depth+1);
            fprintf(stream, "table: %s", n->s);
            print_indent(stream, depth+1);
            fprintf(stream, "data : [");
            print_ast(stream, n->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "]");
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, n->r, depth);
            break;
        }
        case NT_CREATE: {
            struct NStringWChild* n = (struct NStringWChild*)node;
            fprintf(stream, "create : {");
            print_indent(stream, depth+1);
            fprintf(stream, "table: %s", n->s);
            print_indent(stream, depth+1);
            fprintf(stream, "scheme : {");
            print_ast(stream, n->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "}");
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, n->r, depth);
            break;
        }
        case NT_MERGE:
            fprintf(stream, "merge: {");
            print_ast(stream, node->l, depth+1);
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, node->r, depth);
            break;
        case NT_FILTER: {
            fprintf(stream, "filter: {");
            print_ast(stream, node->l, depth+1);
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, node->r, depth);
            break;
        }
        case NT_UPDATE: {
            struct NDStringWChild* n = (struct NDStringWChild*)node;
            fprintf(stream, "update: {");
            print_indent(stream, depth+1);
            fprintf(stream, "table_alias: %s", n->s1);
            print_indent(stream, depth+1);
            fprintf(stream, "table: %s", n->s2);
            print_indent(stream, depth+1);
            fprintf(stream, "data: [");
            print_ast(stream, n->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "]");
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, n->r, depth);
            break;
        }
        case NT_RET:
            fprintf(stream, "return: {");
            print_ast(stream, node->l, depth+1);
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, node->r, depth);
            break;
        case NT_FOR: {
            struct NDStringWChild* n = (struct NDStringWChild*)node;
            fprintf(stream, "for: {");
            print_indent(stream, depth+1);
            fprintf(stream, "table_alias: %s", n->s1);
            print_indent(stream, depth+1);
            fprintf(stream, "table: %s", n->s2);
            print_indent(stream, depth+1);
            fprintf(stream, "body: [");
            print_ast(stream, n->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "]");
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, n->r, depth);
            break;
        }
        case NT_PAIR:
            fprintf(stream, "pair: {");
            print_indent(stream, depth+1);
            fprintf(stream, "key: %s", ((struct NStringWChild*)node)->s);
            print_indent(stream, depth+1);
            fprintf(stream, "value: {");
            print_ast(stream, node->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "}");
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, node->r, depth);
            break;
        case NT_DEF_BOOL:
        case NT_DEF_FLOAT:
        case NT_DEF_INT:
        case NT_DEF_STR:
            fprintf(stream, "definition: {");
            print_indent(stream, depth+1);
            fprintf(stream, "name: '%s'", ((struct NString*)node)->val);
            print_indent(stream, depth+1);
            fprintf(stream, "type: '%s'", get_vartype_name(node->type));
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, node->r, depth);
            break;
        case NT_COND_EQ:
        case NT_COND_NEQ:
        case NT_COND_GR:
        case NT_COND_LE:
        case NT_COND_EGR:
        case NT_COND_ELE:
        case NT_COND_SUBSTR:
            fprintf(stream, "expression: {");
            print_indent(stream, depth+1);
            fprintf(stream, "type: '%s'", get_condition_name(node->type));
            print_indent(stream, depth+1);
            fprintf(stream, "left: {");
            print_ast(stream, node->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "}");
            print_indent(stream, depth+1);
            fprintf(stream, "right: {");
            print_ast(stream, node->r, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "}");
            print_indent(stream, depth);
            fprintf(stream, "}");
            break;
        case NT_COND_AND:
        case NT_COND_OR:
            fprintf(stream, "expression: {");
            print_indent(stream, depth+1);
            fprintf(stream, "type: '%s'", get_condition_name(node->type));
            print_indent(stream, depth+1);
            fprintf(stream, "left: {");
            print_ast(stream, node->l, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "}");
            print_indent(stream, depth+1);
            fprintf(stream, "right: {");
            print_ast(stream, node->r, depth+2);
            print_indent(stream, depth+1);
            fprintf(stream, "}");
            print_indent(stream, depth);
            fprintf(stream, "}");
            break;
        case NT_POINT_OP:
            fprintf(stream, "operator: {");
            print_indent(stream, depth+1);
            fprintf(stream, "type: point");
            print_indent(stream, depth+1);
            fprintf(stream, "left: '%s'", ((struct NDoubleString*)node)->s1);
            print_indent(stream, depth+1);
            fprintf(stream, "right: '%s'", ((struct NDoubleString*)node)->s2);
            print_indent(stream, depth);
            fprintf(stream, "}");
            print_ast(stream, node->r, depth);
            break;
        case NT_FLOAT:
            fprintf(stream, "float literal: %f", ((struct NFloat*)node)->val);
            break;
        case NT_INT:
            fprintf(stream, "int literal: %d", ((struct NInt*)node)->val);
            break;
        case NT_STR:
            fprintf(stream, "string literal: %s", ((struct NString*)node)->val);
            break;
        case NT_BOOL:
            fprintf(stream, "bool literal: %d", ((struct NInt*)node)->val);
            break;
        case NT_VAR:
            fprintf(stream, "table_alias: %s", ((struct NString*)node)->val);
            print_ast(stream, node->r, depth);
            break;
    }
    if (depth == 0)
        fprintf(stream, "\n");
}
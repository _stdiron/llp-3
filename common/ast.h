
#ifndef AST
#define AST

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

enum TNode {
    NT_REMOVE, NT_INSERT, NT_MERGE, NT_FILTER,
    NT_UPDATE, NT_RET, NT_FOR, NT_PAIR, NT_POINT_OP, NT_CREATE,
    // table_alias types
    NT_DEF_INT, NT_DEF_FLOAT, NT_DEF_STR, NT_DEF_BOOL,
    // conditions
    NT_COND_EQ, NT_COND_NEQ, NT_COND_GR, NT_COND_LE,
    NT_COND_EGR, NT_COND_ELE, NT_COND_SUBSTR,
    NT_COND_AND, NT_COND_OR,
    // values
    NT_FLOAT, NT_INT, NT_STR, NT_BOOL, NT_VAR,
    NT_PARSE_ERROR
};

// Condition, Conditions
typedef struct Ast {
    enum TNode type;
    struct Ast* l;
    struct Ast* r;
} Ast;

// For, Update
typedef struct NDStringWChild {
    enum TNode type;
    struct Ast* l;
    struct Ast* r;      // next expr
    char* s1;
    char* s2;
} NDStringWChild;

// Insert, Pair
typedef struct NStringWChild {
    enum TNode type;
    struct Ast* l;
    struct Ast* r;      // next expr
    char* s;
} NStringWChild;

typedef struct NFloat {
    enum TNode type;
    float val;
} NFloat;

typedef struct NInt {
    enum TNode type;
    int val;
} NInt;

typedef struct NString {
    enum TNode type;
    char* val;
    struct Ast* r;          // next expr
} NString;

// Remove
typedef struct NDoubleString {
    enum TNode type;
    char* s1;
    struct Ast* r;          // next expr
    char* s2;
} NDoubleString;

struct Ast* create_node(int type, struct Ast* l, struct Ast* r);
struct Ast* create_oc_node(int type, struct Ast* child);
struct Ast* create_int_node(int val);
struct Ast* create_float_node(float val);
struct Ast* create_string_node(char* val, int type);
struct Ast* create_bool_node(int val);
struct Ast* create_string_child_node(int type, char* s, struct Ast* l);
struct Ast* create_dstring_child_node(int type, char* s1, char* s2, struct Ast* l);
struct Ast* create_dstring_node(int type, char* s1, char* s2);
void add_at_end(struct Ast* root, struct Ast* node);

void print_ast(FILE* stream, struct Ast* node, int depth);
void free_ast(struct Ast* node);

#endif 
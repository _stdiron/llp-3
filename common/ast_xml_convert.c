//
// Created by vlad on 31.12.23.
//
#include <xmlrpc.h>
#include <stdio.h>
#include "ast_xml_convert.h"

void xmlrpc_check_fault(xmlrpc_env* env) {
    if (env->fault_occurred) {
        fprintf(stderr, "ERROR: %s (%d)\n",env->fault_string, env->fault_code);
        exit(3);
    }
}

xmlrpc_value* create_xml_node(enum TNode type, xmlrpc_env* env) {
    xmlrpc_value* xml_node = xmlrpc_struct_new(env);
    xmlrpc_check_fault(env);
    xmlrpc_value* type_v = xmlrpc_int_new(env, type);
    xmlrpc_struct_set_value(env,xml_node,"type",type_v);
    xmlrpc_DECREF(type_v);
    return xml_node;
}

void xml_set_child(Ast* child, char* key, xmlrpc_value* xml_node, xmlrpc_env* env) {
    xmlrpc_value* next = build_xmlrpc_from_ast(child, env);
    xmlrpc_struct_set_value(env,xml_node,key, next);
    xmlrpc_DECREF(next);
}

void xml_set_next(Ast* node, xmlrpc_value* xml_node, xmlrpc_env* env) {
    if (node->r == NULL)
        return;
    xml_set_child(node->r, "next", xml_node, env);
}

void xml_set_string(char* val, char* key, xmlrpc_value* xml_node, xmlrpc_env* env) {
    xmlrpc_value* value = xmlrpc_string_new(env, val);
    xmlrpc_struct_set_value(env, xml_node, key, value);
    xmlrpc_DECREF(value);
}

xmlrpc_value* build_xmlrpc_from_ast(Ast* node, xmlrpc_env* env) {
    xmlrpc_value* xml_node;
    switch (node->type) {
        case NT_INSERT:
        case NT_CREATE:
        case NT_PAIR: {
            struct NStringWChild* n = (struct NStringWChild*)node;
            xml_node = create_xml_node(node->type, env);
            xml_set_child(node->l, "child", xml_node, env);
            xml_set_string(n->s, "string", xml_node, env);
            xml_set_next(node, xml_node, env);
            break;
        }
        case NT_UPDATE:
        case NT_FOR: {
            struct NDStringWChild* n = (struct NDStringWChild*)node;
            xml_node = create_xml_node(node->type, env);
            xml_set_child(node->l, "child", xml_node, env);
            xml_set_string(n->s1, "string_1", xml_node, env);
            xml_set_string(n->s2, "string_2", xml_node, env);
            xml_set_next(node, xml_node, env);
            break;
        }
        case NT_MERGE:
        case NT_FILTER:
        case NT_RET: {
            xml_node = create_xml_node(node->type, env);
            xml_set_child(node->l, "child", xml_node, env);
            xml_set_next(node, xml_node, env);
            break;
        }
        case NT_COND_EQ:
        case NT_COND_NEQ:
        case NT_COND_GR:
        case NT_COND_LE:
        case NT_COND_EGR:
        case NT_COND_ELE:
        case NT_COND_SUBSTR:
        case NT_COND_AND:
        case NT_COND_OR: {
            xml_node = create_xml_node(node->type, env);
            xml_set_child(node->l, "l_child", xml_node, env);
            xml_set_child(node->r, "r_child", xml_node, env);
            break;
        }
        case NT_REMOVE:
        case NT_POINT_OP: {
            struct NDoubleString* n = (struct NDoubleString*)node;
            xml_node = create_xml_node(node->type, env);
            xml_set_string(n->s1, "string_1", xml_node, env);
            xml_set_string(n->s2, "string_2", xml_node, env);
            xml_set_next(node, xml_node, env);
            break;
        }
        case NT_STR:
        case NT_DEF_BOOL:
        case NT_DEF_FLOAT:
        case NT_DEF_INT:
        case NT_DEF_STR:
        case NT_VAR: {
            struct NString* n = (struct NString*)node;
            xml_node = create_xml_node(node->type, env);
            xml_set_string(n->val, "value", xml_node, env);
            xml_set_next(node, xml_node, env);
            break;
        }
        case NT_FLOAT: {
            xml_node = create_xml_node(node->type, env);
            xmlrpc_value* value = xmlrpc_double_new(env, (double)((NFloat*)node)->val);
            xmlrpc_struct_set_value(env, xml_node, "value", value);
            xmlrpc_DECREF(value);
            break;
        }
        case NT_INT: {
            xml_node = create_xml_node(node->type, env);
            xmlrpc_value* value = xmlrpc_int_new(env, ((NInt*)node)->val);
            xmlrpc_struct_set_value(env, xml_node, "value", value);
            xmlrpc_DECREF(value);
            break;
        }
        case NT_BOOL: {
            xml_node = create_xml_node(node->type, env);
            xmlrpc_value* value = xmlrpc_int_new(env, ((NInt*)node)->val);
            xmlrpc_struct_set_value(env, xml_node, "value", value);
            xmlrpc_DECREF(value);
            break;
        }
    }
    xmlrpc_check_fault(env);
    return xml_node;
}

static int m_xmlrpc_get_int(xmlrpc_value* xml_node, char* key, xmlrpc_env* env) {
    int result = -1;
    xmlrpc_value* xml_val;
    xmlrpc_struct_read_value(env, xml_node, key, &xml_val);
    xmlrpc_read_int(env, xml_val, &result);
    xmlrpc_DECREF(xml_val);
    return result;
}

static float m_xmlrpc_get_float(xmlrpc_value* xml_node, char* key, xmlrpc_env* env) {
    double result = -1;
    xmlrpc_value* xml_val;
    xmlrpc_struct_read_value(env, xml_node, key, &xml_val);
    xmlrpc_read_double(env, xml_val, &result);
    xmlrpc_DECREF(xml_val);
    return (float)result;
}

static char* m_xmlrpc_get_str(xmlrpc_value* xml_node, char* key, xmlrpc_env* env) {
    char* str = NULL;
    xmlrpc_value* xml_val;
    xmlrpc_struct_read_value(env, xml_node, key, &xml_val);
    xmlrpc_read_string(env, xml_val, (const char**)&str);
    xmlrpc_DECREF(xml_val);
    return str;
}

static Ast* m_xmlrpc_parse_child(xmlrpc_value* xml_node, char* key, uint8_t required, xmlrpc_env* env) {
    xmlrpc_value* xml_val;
    xmlrpc_struct_find_value(env, xml_node, key, &xml_val);
    if (required && !xml_val) {
        return create_node(NT_PARSE_ERROR, 0, 0);
    }
    Ast* node = build_ast_from_xmlrpc(xml_val, env);
    if (xml_val)
        xmlrpc_DECREF(xml_val);
    return node;
}

Ast* build_ast_from_xmlrpc(xmlrpc_value* xml_node, xmlrpc_env* env) {
    if (!xml_node)
        return NULL;
    int type = m_xmlrpc_get_int(xml_node, "type", env);
    switch (type) {
        case NT_INSERT:
        case NT_CREATE:
        case NT_PAIR: {
            NStringWChild* n = (NStringWChild*)create_string_child_node(type, 0, 0);
            n->s = m_xmlrpc_get_str(xml_node, "string", env);
            n->l = m_xmlrpc_parse_child(xml_node, "child", 1, env);
            n->r = m_xmlrpc_parse_child(xml_node, "next", 0, env);
            return (Ast*)n;
        }
        case NT_UPDATE:
        case NT_FOR: {
            struct NDStringWChild* n = (NDStringWChild*)create_dstring_child_node(type, 0, 0, 0);
            n->l = m_xmlrpc_parse_child(xml_node, "child", 1, env);
            n->s1 = m_xmlrpc_get_str(xml_node, "string_1", env);
            n->s2 = m_xmlrpc_get_str(xml_node, "string_2", env);
            n->r = m_xmlrpc_parse_child(xml_node, "next", 0, env);
            return (Ast*)n;
        }
        case NT_MERGE:
        case NT_FILTER:
        case NT_RET: {
            struct Ast* n = create_node(type, 0, 0);
            n->l = m_xmlrpc_parse_child(xml_node, "child", 1, env);
            n->r = m_xmlrpc_parse_child(xml_node, "next", 0, env);
            return n;
        }
        case NT_COND_EQ:
        case NT_COND_NEQ:
        case NT_COND_GR:
        case NT_COND_LE:
        case NT_COND_EGR:
        case NT_COND_ELE:
        case NT_COND_SUBSTR:
        case NT_COND_AND:
        case NT_COND_OR: {
            struct Ast* n = create_node(type, 0, 0);
            n->l = m_xmlrpc_parse_child(xml_node, "l_child", 1, env);
            n->r = m_xmlrpc_parse_child(xml_node, "r_child", 1, env);
            return n;
        }
        case NT_REMOVE:
        case NT_POINT_OP: {
            struct NDoubleString* n = (NDoubleString*)create_dstring_node(type, 0, 0);
            n->s1 = m_xmlrpc_get_str(xml_node, "string_1", env);
            n->s2 = m_xmlrpc_get_str(xml_node, "string_2", env);
            n->r = m_xmlrpc_parse_child(xml_node, "next", 0, env);
            return (Ast*)n;
        }
        case NT_STR:
        case NT_DEF_BOOL:
        case NT_DEF_FLOAT:
        case NT_DEF_INT:
        case NT_DEF_STR:
        case NT_VAR: {
            NString* n = (NString*)create_string_node(
                    m_xmlrpc_get_str(xml_node, "value", env),
                    type
            );
            n->r = m_xmlrpc_parse_child(xml_node, "next", 0, env);
            return (Ast*)n;
        }
        case NT_FLOAT:
            return (Ast*)create_float_node(m_xmlrpc_get_float(xml_node, "value", env));
        case NT_INT:
            return (Ast*)create_int_node(m_xmlrpc_get_int(xml_node, "value", env));
        case NT_BOOL:
            return (Ast*)create_bool_node(m_xmlrpc_get_int(xml_node, "value", env));
        default:
            fprintf(stderr, "cannot find 'type' tag in xml!");
            return create_node(NT_PARSE_ERROR, 0, 0);
    }
}
//
// Created by vlad on 31.12.23.
//

#ifndef LAB3_AST_XML_CONVERT_H
#define LAB3_AST_XML_CONVERT_H

#include "ast.h"

xmlrpc_value* build_xmlrpc_from_ast(Ast* node, xmlrpc_env* env);
Ast* build_ast_from_xmlrpc(xmlrpc_value* xml_node, xmlrpc_env* env);

void xmlrpc_check_fault(xmlrpc_env* env);

#endif //LAB3_AST_XML_CONVERT_H

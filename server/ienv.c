//
// Created by vlad on 06.01.24.
//

#include <malloc.h>
#include <stdlib.h>
#include "ienv.h"
#include "database/util.h"

/*
 * FORMAT FOR RESPONSE
 * xml-array [
 *      RESPONSE_TYPE,
 *      SI_TYPE / error message,
 *      *next for non-error*
 *      scheme,
 *      *next only for operations request*
 *      number of elements affected,
 *      *next only for select operations request*
 *      rows [...]
 *      id_to_get_next or -1 if you cant...
 * ]
 */

IEnv* create_ienv() {
    IEnv* env = malloc(sizeof(IEnv));
    env->response = 0;
    env->error = 0;
    env->operation = 0;
    env->intent = SI_NOTHING;
    env->finished = 0;
    return env;
}

void free_ienv(IEnv* env, Storage* storage) {
    if (env->intent == SI_GET || env->intent == SI_UPDATE || env->intent == SI_REMOVE)
        free_operation(env->operation, 1);
    if ((env->intent == SI_INSERT || env->intent == SI_CREATE) && env->table) {
        close_table(storage, env->table);
        free(env->table);
    }
    free(env);
}

static void xmlrpc_check_fault(xmlrpc_env* env) {
    if (env->fault_occurred) {
        fprintf(stderr, "ERROR: %s (%d)\n",env->fault_string, env->fault_code);
        exit(7);
    }
}

void make_error_response(IEnv* env, xmlrpc_env* rpc_env) {
    env->response = xmlrpc_build_value(rpc_env, "(is)", RESPONSE_ERROR, env->error);
    xmlrpc_check_fault(rpc_env);
}

xmlrpc_value* make_error_response_string(char* message, xmlrpc_env* rpc_env) {
    return xmlrpc_build_value(rpc_env, "(is)", RESPONSE_ERROR, message);
}

xmlrpc_value* scheme_to_xmlrpc_array(SchemeItem* scheme, uint16_t n, xmlrpc_env* rpc_env) {
    xmlrpc_value* xml_array = xmlrpc_array_new(rpc_env);
    for (uint16_t i = 0; i < n; i++) {
        xmlrpc_value* field = xmlrpc_build_value(rpc_env, "(si)", scheme[i].name, scheme[i].type);
        xmlrpc_array_append_item(rpc_env, xml_array, field);
        xmlrpc_check_fault(rpc_env);
        xmlrpc_DECREF(field);
    }
    return xml_array;
}

static RowsIterator* get_iterator_with_column(RowsIterator* iter, int index) {
    while (iter) {
        if (index >= iter->row_offset && index < iter->row_size)
            return iter;
        iter = iter->outer;
    }
    return NULL;
}

xmlrpc_value* row_to_xmlrpc_array(void** row, SchemeItem* scheme, int n, xmlrpc_env* rpc_env) {
    xmlrpc_value* row_xml = xmlrpc_array_new(rpc_env);
    for (int i = 0; i < n; i++) {
        void* value = row[scheme[i].order];
        xmlrpc_value* xml_value;
        switch (scheme[i].type) {
            case TABLE_FTYPE_INT_32:
                xml_value = xmlrpc_int_new(rpc_env, *(int32_t*)value);
                xmlrpc_array_append_item(rpc_env, row_xml, xml_value);
                break;
            case TABLE_FTYPE_FLOAT: {
                double double_val = (double)(*(float*)value);
                xml_value = xmlrpc_double_new(rpc_env, double_val);
                xmlrpc_array_append_item(rpc_env, row_xml, xml_value);
                break;
            }
            case TABLE_FTYPE_BOOL: {
                int int_val = (int)(*(uint8_t*)value);
                xml_value = xmlrpc_int_new(rpc_env, int_val);
                xmlrpc_array_append_item(rpc_env, row_xml, xml_value);
                break;
            }
            case TABLE_FTYPE_STRING:
                xml_value = xmlrpc_string_new(rpc_env, (char*)value);
                xmlrpc_array_append_item(rpc_env, row_xml, xml_value);
                break;
            default:
                panic("unexpected column datatype", 8);
                break;
        }
        xmlrpc_check_fault(rpc_env);
        xmlrpc_DECREF(xml_value);
    }
    return row_xml;
}

void make_select_operation_response(IEnv* env, xmlrpc_env* rpc_env) {
    Operation* op = env->operation;
    if (env->intent != SI_GET || op->intent != SI_GET)
        panic("incompatible response type", 8);
    if (op->selection_mask == NULL)
        panic("selection mask is Null", 8);

    RowsIterator* current_iter = 0;
    int columns_to_get_n = 0;
    for (uint16_t i = 0; i < op->iterator->row_size; i++) {
        if (op->selection_mask[i] == 1)
            columns_to_get_n++;
    }
    TableScheme scheme_of_selected = create_table_scheme(columns_to_get_n);
    for (uint16_t i = 0; i < op->iterator->row_size; i++) {
        if (op->selection_mask[i] == 0)
            continue;
        if (!current_iter || current_iter->row_offset > i || i >= current_iter->row_size)
            current_iter = get_iterator_with_column(op->iterator, i);
        if (!current_iter)
            panic("cant find iterator of column", 8);
        SchemeItem* scheme_item = current_iter->table->scheme + (i - current_iter->row_offset);
        add_scheme_field(&scheme_of_selected, scheme_item->name, scheme_item->type, 1);
        scheme_of_selected.fields[scheme_of_selected.size-1].order = i;
    }

    int counter = 0;
    xmlrpc_value* scheme_xml = scheme_to_xmlrpc_array(scheme_of_selected.fields,
                                                      scheme_of_selected.size, rpc_env);
    xmlrpc_value* rows_array = xmlrpc_array_new(rpc_env);
    env->finished = 1;
    while (rows_iterator_next(op->iterator) == REQUEST_ROW_FOUND) {
        xmlrpc_value* row_xml = row_to_xmlrpc_array(op->iterator->row, scheme_of_selected.fields,
                                                    scheme_of_selected.size, rpc_env);
        xmlrpc_array_append_item(rpc_env, rows_array, row_xml);
        xmlrpc_check_fault(rpc_env);
        xmlrpc_DECREF(row_xml);
        if (++counter == ROWS_PER_RESPONSE) {
            env->finished = 0;
            break;
        }
    }
    xmlrpc_check_fault(rpc_env);
    env->response = xmlrpc_build_value(rpc_env, "(iiAiA)",
                                       RESPONSE_DATA, op->intent, scheme_xml, counter, rows_array);
    xmlrpc_check_fault(rpc_env);
    xmlrpc_DECREF(rows_array);
    xmlrpc_DECREF(scheme_xml);
    free_scheme(scheme_of_selected.fields, scheme_of_selected.size);
}

void make_operation_response(IEnv* env, xmlrpc_env* rpc_env) {
    Operation* op = env->operation;
    if (op->intent == SI_UPDATE && op->update_row == 0)
        panic("null update row!", 8);
    if (op->intent == SI_GET) {
        make_select_operation_response(env, rpc_env);
    } else {
        int counter = 0;
        while (rows_iterator_next(op->iterator) == REQUEST_ROW_FOUND) {
            if (op->intent == SI_REMOVE) {
                rows_iterator_remove_current(op->iterator);
            } else if (op->intent == SI_UPDATE) {
                rows_iterator_update_current(op->iterator, op->update_row);
            } else {
                panic("cant handle this intent here...", 8);
            }
            counter++;
        }
        xmlrpc_value* scheme_xml = scheme_to_xmlrpc_array(op->iterator->table->scheme,
                                                          op->iterator->table->mapped_addr->fields_n, rpc_env);
        xmlrpc_check_fault(rpc_env);
        env->response = xmlrpc_build_value(rpc_env, "(iiAi)", RESPONSE_OK, op->intent, scheme_xml, counter);
        xmlrpc_check_fault(rpc_env);
        xmlrpc_DECREF(scheme_xml);
        env->finished = 1;
    }
}

void make_create_response(IEnv* env, xmlrpc_env* rpc_env) {
    if (env->intent != SI_CREATE)
        panic("incompatible response type", 8);
    xmlrpc_value* scheme_array = scheme_to_xmlrpc_array(env->table->scheme,
                                                        env->table->mapped_addr->fields_n, rpc_env);
    env->response = xmlrpc_build_value(rpc_env, "(iiA)", RESPONSE_OK, SI_CREATE, scheme_array);
    xmlrpc_check_fault(rpc_env);
    xmlrpc_DECREF(scheme_array);
}

void make_insert_response(IEnv* env, xmlrpc_env* rpc_env) {
    if (env->intent != SI_INSERT)
        panic("incompatible response type", 8);
    xmlrpc_value* scheme_array = scheme_to_xmlrpc_array(env->table->scheme,
                                                        env->table->mapped_addr->fields_n, rpc_env);
    env->response = xmlrpc_build_value(rpc_env, "(iiA)", RESPONSE_OK, SI_INSERT, scheme_array);
    xmlrpc_check_fault(rpc_env);
    xmlrpc_DECREF(scheme_array);
}

void make_response(IEnv* env, xmlrpc_env* rpc_env) {
    if (env->error) {
        make_error_response(env, rpc_env);
        return;
    }
    switch (env->intent) {
        case SI_NOTHING:
            env->response = xmlrpc_build_value(rpc_env, "(ii)", RESPONSE_OK, SI_NOTHING);
            env->finished = 1;
            break;
        case SI_REMOVE:
        case SI_GET:
        case SI_UPDATE:
            make_operation_response(env, rpc_env);
            break;
        case SI_INSERT:
            make_insert_response(env, rpc_env);
            env->finished = 1;
            break;
        case SI_CREATE:
            make_create_response(env, rpc_env);
            env->finished = 1;
            break;
    }
}
//
// Created by vlad on 06.01.24.
//

#ifndef LAB3_IENV_H
#define LAB3_IENV_H


#include "xmlrpc.h"
#include "database/storage.h"
#include "interpreter.h"

#define ROWS_PER_RESPONSE 100

typedef struct Operation Operation;

typedef struct IEnv {
    char* error;
    int intent;
    xmlrpc_value* response;   // op type, scheme, content
    union {
        Operation* operation;   // for SI_REMOVE, SI_GET, SI_UPDATE
        OpenedTable* table;     // for SI_CREATE
    };
    uint8_t finished;
} IEnv;

enum ResponseType {
    RESPONSE_ERROR,
    RESPONSE_DATA,
    RESPONSE_OK
};

IEnv* create_ienv();
void free_ienv(IEnv* env, Storage* storage);

void make_response(IEnv* env, xmlrpc_env* rpc_env);
xmlrpc_value* make_error_response_string(char* message, xmlrpc_env* rpc_env);

#endif //LAB3_IENV_H

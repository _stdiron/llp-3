//
// Created by vlad on 02.01.24.
//

#include "interpreter.h"

static void ast_build_scheme(Ast* node, TableScheme* scheme, IEnv* env);
static Operation* find_operation_by_alias(Operation* op, char* table_alias);
static FilterNode* ast_build_filter(Ast* node, Operation* op, IEnv* env);
static int ast_build_row(NStringWChild* node, OpenedTable* table, void* row[], IEnv* env);

static TableDatatype get_leaf_value(Ast* node, void** ret_val);
static int try_open_table(IEnv* env, Storage* storage, char* name, OpenedTable* table);
static void* duplicate_value(void* value, TableDatatype type);

Operation* build_operation_from_ast(Ast* node, RowsIterator* iter, char* alias, Operation* parent, IEnv* env);

static filter_predicate cond_type_filter_map[] = {
        [NT_COND_EQ] = equals_filter,
        [NT_COND_NEQ] = not_equals_filter,
        [NT_COND_GR] = greater_filter,
        [NT_COND_EGR] = greater_eq_filter,
        [NT_COND_LE] = less_filter,
        [NT_COND_ELE] = less_eq_filter,
        [NT_COND_SUBSTR] = substring_of,
};

IEnv* interpret_root(Ast* node, Storage* storage) {
    IEnv* env = create_ienv();
    if (node == NULL) {
        env->error = "passed null-node (probably weren't any input operations)";
        return env;
    }
    switch (node->type) {
        case NT_CREATE: {
            TableScheme scheme;
            env->table = malloc(sizeof(OpenedTable));
            ast_build_scheme(node->l, &scheme, env);
            if (env->error)
                break;
            char* table_name = ((NStringWChild*)node)->s;
            if (open_table(storage, table_name, env->table)) {
                env->error = "table with the same name already exists in storage!";
                break;
            }
            Table* ttable = init_table(&scheme, table_name);
            create_table(storage, ttable, env->table);
            env->intent = SI_CREATE;
            destruct_table(ttable);
            break;
        }
        case NT_INSERT: {
            NStringWChild* n = (NStringWChild*)node;
            env->table = malloc(sizeof(OpenedTable));
            if (!try_open_table(env, storage, n->s, env->table)) {
                free(env->table);
                env->table = 0;
                break;
            }
            int fields_n = env->table->mapped_addr->fields_n;
            void** row = malloc(sizeof(void*) * fields_n);
            memset(row, 0, sizeof(void*) * fields_n);
            int row_items_filled = ast_build_row((NStringWChild*)n->l, env->table, row, env);
            if (row_items_filled != fields_n || env->error) {
                if (!env->error)
                    env->error = "the values of each of the columns must be given";
//                free_row_content(fields_n, row);
                free(row);
                break;
            }
            table_insert_row(storage, env->table, row);
            env->intent = SI_INSERT;
            free(row);
            break;
        }
        case NT_FOR: {
            NDStringWChild* n = (NDStringWChild*)node;
            OpenedTable* table = malloc(sizeof(OpenedTable));
            if (!try_open_table(env, storage, n->s2, table)) {
                free(table);
                break;
            }
            env->operation = build_operation_from_ast(
                    n->l,
                    create_rows_iterator(storage, table),
                    n->s1,
                    NULL,
                    env
             );
            env->intent = env->operation->intent;
            break;
        }
        default:
            env->error = "wrong node passed into interpret_root function";
            break;
    }
    return env;
}

static void ast_op_filter(Ast* node, Operation* op, IEnv* env) {
    FilterNode* current_filter = rows_iterator_get_filter(op->iterator);
    FilterNode* new_filter = ast_build_filter(node->l, op, env);
    if (!current_filter) {
        rows_iterator_set_filter(op->iterator, new_filter);
        return;
    }
    rows_iterator_set_filter(
        op->iterator,
        create_filter_and(current_filter,new_filter)
    );
}

static void ast_op_remove(Ast* node, Operation* op, IEnv* env) {
    NDoubleString* n = (NDoubleString*)node;
    if (strcmp(op->table_alias, n->s1) != 0
        || strcmp(op->iterator->table->mapped_addr->name, n->s2) != 0) {
        env->error = "removal in sub-scopes isn't supported";
    }
    op->intent = SI_REMOVE;
}

static int write_selection_mask(Operation* op, char* varname) {
    RowsIterator* found_iter = 0;
    Operation* cur_op = op;
    while (cur_op) {
        if (strcmp(varname, cur_op->table_alias) == 0) {
            found_iter = cur_op->iterator;
            break;
        }
        cur_op = cur_op->parent;
    }
    if (found_iter == NULL)
        return 0;
    memset(op->selection_mask + found_iter->row_offset, 1, found_iter->table->mapped_addr->fields_n);
    return 1;
}

static void ast_op_return(Ast* node, Operation* op, IEnv* env) {
    Ast* n = (node->l);
    op->intent = SI_GET;
    op->selection_mask = malloc(op->iterator->row_size);
    memset(op->selection_mask, 0, op->iterator->row_size);
    if (n->type == NT_VAR) {
        if (!write_selection_mask(op, ((NString*)n)->val))
            env->error = "undeclared alias used in return statement!";
    } else if (n->type == NT_MERGE) {
        n = n->l;       // get table_alias node
        if (n->type != NT_VAR) {    // we can interpret only statement like RETURN(v1, v2)
                                    // not RETURN(v1.field, v2.field2)
            env->error = "incompatible merge arguments";
            return;
        }
        while (n) {
            if (!write_selection_mask(op, ((NString*)n)->val)) {
                env->error = "undeclared alias used in return statement!";
                return;
            }
            n = n->r;
        }
    } else {
        env->error = "incompatible return arguments";
        return;
    }
}

// support update only in current scope table
// and only literals as an argument
static void ast_op_update(Ast* node, Operation* op, IEnv* env) {
    NDStringWChild* n = (NDStringWChild*)node;
    if (strcmp(op->table_alias, n->s1) != 0
            || strcmp(op->iterator->table->mapped_addr->name, n->s2) != 0) {
        env->error = "probably table you're trying to update is non-existent or defined in different scope";
        return;
    }
    op->intent = SI_UPDATE;
    op->update_row = malloc(sizeof(void*) * op->iterator->table->mapped_addr->fields_n);
    memset(op->update_row, 0, op->iterator->table->mapped_addr->fields_n * sizeof(void*));
    NStringWChild* n_pair = (NStringWChild*)n->l;
    while (n_pair) {
        if (n_pair->type != NT_PAIR) {
            env->error = "wrong expression passed into the update statement";
            return;
        }
        int index = table_find_index_of_column(op->iterator->table, n_pair->s);
        if (index < 0) {
            env->error = "used non-existent column name in update statement";
            return;
        }
        TableDatatype col_type = get_leaf_value(n_pair->l, op->update_row+index);
        if (col_type != op->iterator->table->scheme[index].type) {
            env->error = "incompatible update value type";
            return;
        }
        n_pair = (NStringWChild*)n_pair->r;
    }
}

// when we got Operation object, we should
// construct using current scope instructions
Operation* build_operation_from_ast(Ast* node, RowsIterator* iter, char* alias, Operation* parent, IEnv* env) {
    Operation* op = malloc(sizeof(Operation));
    op->iterator = iter;
    op->parent = parent;
    op->intent = SI_NOTHING;
    op->table_alias = strdup(alias);
    op->selection_mask = NULL;

    while (node) {
        if (op->intent != SI_NOTHING)
            panic("operation already has an intent!", 7);
        switch (node->type) {
            case NT_FOR: {
                NDStringWChild* n = (NDStringWChild*)node;
                OpenedTable* table = malloc(sizeof(OpenedTable));
                if (!try_open_table(env, op->iterator->storage, n->s2, table)) {
                    env->error = "cant open the table";
                    break;
                }
                Operation* new_op = build_operation_from_ast(
                        n->l,
                        create_rows_iterator_inner(table, op->iterator),
                        n->s1,
                        op,
                        env
                );
                if (new_op->intent != SI_NOTHING) {
                    free_operation(op, 0);
                    return new_op;
                } else {
                    rows_iterator_free(new_op->iterator);
                    free_operation(new_op, 0);
                }
                break;
            }
            case NT_FILTER:
                ast_op_filter(node, op, env);
                break;
            case NT_REMOVE:
                ast_op_remove(node, op, env);
                return op;
            case NT_RET:
                ast_op_return(node, op, env);
                return op;
            case NT_UPDATE:
                ast_op_update(node, op, env);
                return op;
            default:
                env->error = "cant interpret it here!";
                break;
        }
        if (env->error)
            return op;
        node = node->r;
    }
    return op;
}

// return number of built fields
static int ast_build_row(NStringWChild* node, OpenedTable* table, void* row[], IEnv* env) {
    if (!node)
        return 0;
    if (node->type != NT_PAIR) {
        env->error = "insert statement wrong syntax";
        return 0;
    }
    int index = table_find_index_of_column(table, node->s);
    if (index < 0) {
        env->error = "used non-existent column name in insert statement";
        return 0;
    }
    if (get_leaf_value(node->l, row+index) != table->scheme[index].type) {
        env->error = "incompatible type of insert argument";
        return 0;
    }
    return ast_build_row((NStringWChild*)node->r, table, row, env) + 1;
}

static TableDatatype get_leaf_value(Ast* node, void** ret_val) {
    if (!node)
        panic("passed NULL leaf", 7);
    switch (node->type) {
        case NT_INT:
            *ret_val = &((NInt*)node)->val;
            return TABLE_FTYPE_INT_32;
        case NT_FLOAT:
            *ret_val = &((NFloat*)node)->val;
            return TABLE_FTYPE_FLOAT;
        case NT_STR:
            *ret_val = ((NString*)node)->val;
            return TABLE_FTYPE_STRING;
        case NT_BOOL: {
            //            ((NInt*)node)->val = 1 - (((NInt*)node)->val % 1);
            uint32_t u32 = (uint32_t)((NInt*)node)->val;
            memcpy(&((NInt*)node)->val, &u32, 4);
            *ret_val = &((NInt*)node)->val;
            return TABLE_FTYPE_BOOL;
        }
        default:
            panic("not a value-leaf passed into get_leaf_value", 7);
    }
}

int ast_get_depth(Ast* node) {
    if (!node)
        return 0;
    return ast_get_depth(node->r) + 1;
}

static void ast_build_scheme(Ast* node, TableScheme* scheme, IEnv* env) {
    if (!node) {
        env->error = "null node passed into build_scheme";
        return;
    }
    *scheme = create_table_scheme(ast_get_depth(node));
    NString* n = (NString*)node;
    while (n) {
        switch (n->type) {
            case NT_DEF_INT:
                add_scheme_field(scheme, n->val, TABLE_FTYPE_INT_32, 1);
                break;
            case NT_DEF_STR:
                add_scheme_field(scheme, n->val, TABLE_FTYPE_STRING, 1);
                break;
            case NT_DEF_FLOAT:
                add_scheme_field(scheme, n->val, TABLE_FTYPE_FLOAT, 1);
                break;
            case NT_DEF_BOOL:
                add_scheme_field(scheme, n->val, TABLE_FTYPE_BOOL, 1);
                break;
            default:
                env->error = "wrong node passed into build_scheme";
                free_scheme(scheme->fields, scheme->size);
                return;
        }
        n = (NString*)n->r;
    }
}

static Operation* find_operation_by_alias(Operation* op, char* table_alias) {
    Operation* cur_op = op;
    while (cur_op) {
        if (strcmp(table_alias, cur_op->table_alias) == 0)
            return cur_op;
        cur_op = cur_op->parent;
    }
    return NULL;
}

static uint8_t node_is_val(Ast* node) {
    return (node->type >= NT_FLOAT && node->type <= NT_BOOL);
}

static FilterNode* create_predicate_node(Ast* node, Operation* op, filter_predicate predicate, IEnv* env) {
    if (!node->r || !node->l ||
        (!node_is_val(node->r) && node->r->type != NT_POINT_OP) ||
        (!node_is_val(node->l) && node->l->type != NT_POINT_OP)) {
        env->error = "incompatible values used in conditions";
        return 0;
    }
    TableDatatype datatype;
    void* l;
    void* r;
    if (node_is_val(node->l)) {
        // assign l
        datatype = get_leaf_value(node->l, &l);
        l = duplicate_value(l, datatype);
        if (node_is_val(node->r)) {
            // *if both values are literals*
            // assign r
            if (datatype != get_leaf_value(node->r, &r)) {
                env->error = "incompatible types in comparison";
                return 0;
            }
            r = duplicate_value(r, datatype);
            return create_filter_node_ll(predicate, datatype, l, r);
        } else {
            // *if node->l - literal, node->r - variable*
            Operation* called_op = find_operation_by_alias(op, ((NDoubleString*)node->r)->s1);
            if (!called_op) {
                env->error = "alias (used in filter) wasn't declared previously";
                return 0;
            }
            // assign r (l assigned)
            r = ((NDoubleString*)node->r)->s2;
            FilterLeaf* leaf = (FilterLeaf*)create_filter_node_l(called_op->iterator, predicate,
                                                                 (char*)r, l, 1, 1);
            if (!leaf) {
                env->error = "there are no column with name used in condition";
                return 0;
            }
            if (leaf->datatype != datatype)
                env->error = "incompatible types used in filter comparison";
            return (FilterNode*)leaf;
        }
    } else {
        Operation* called_op = find_operation_by_alias(op, ((NDoubleString*)node->l)->s1);
        if (!called_op) {
            env->error = "alias (used in filter) wasn't declared previously";
            return 0;
        }
        // assign l
        l = ((NDoubleString*)node->l)->s2;
        if (node_is_val(node->r)) {
            // *if node->l - variable, node->r - literal*
            // assign r (l assigned)
            datatype = get_leaf_value(node->r, &r);
            r = duplicate_value(r, datatype);
            FilterLeaf* leaf = (FilterLeaf*)create_filter_node_l(called_op->iterator, predicate,
                                                                 (char*)l, r, 0, 1);
            if (!leaf) {
                env->error = "there is no column with name used in condition";
                return 0;
            }
            if (leaf->datatype != datatype)
                env->error = "incompatible types used in filter comparison";
            return (FilterNode*)leaf;
        } else {
            // *if both values are variables*
            Operation* called_op2 = find_operation_by_alias(op, ((NDoubleString*)node->r)->s1);
            if (!called_op2) {
                env->error = "alias (used in filter) wasn't declared previously";
                return 0;
            }
            r = ((NDoubleString*)node->r)->s2;
            FilterNode* filter = create_filter_node_v(called_op->iterator, called_op2->iterator, predicate,
                                        (char*)l, (char*)r, 0);
            if (!filter)
                env->error = "incompatible types used in filter comparison or columns doesn't exist";
            return filter;
        }
    }
}

static FilterNode* ast_build_filter(Ast* node, Operation* op, IEnv* env) {
    if (!node) {
        env->error = "null node in filters builder";
        return 0;
    }
    switch (node->type) {
        case NT_COND_EQ:
        case NT_COND_NEQ:
        case NT_COND_GR:
        case NT_COND_EGR:
        case NT_COND_LE:
        case NT_COND_ELE:
        case NT_COND_SUBSTR:
            return create_predicate_node(node, op, cond_type_filter_map[node->type], env);
        case NT_COND_OR:
            return create_filter_or(
                    ast_build_filter(node->l, op, env),
                    ast_build_filter(node->r, op, env)
            );
        case NT_COND_AND:
            return create_filter_and(
                    ast_build_filter(node->l, op, env),
                    ast_build_filter(node->r, op, env)
            );
        default:
            env->error = "wrong node type in condition chain";
            return 0;
    }
}

static int try_open_table(IEnv* env, Storage* storage, char* name, OpenedTable* table) {
    if (!open_table(storage, name, table)) {
        env->error = "table with specified name not found!";
        return 0;
    }
    return 1;
}

static void* duplicate_value(void* value, TableDatatype type) {
    if (type == TABLE_FTYPE_STRING) {
        return strdup(value);
    }
    void* mem = malloc(get_datatype_size(type));
    memcpy(mem, value, get_datatype_size(type));
    return mem;
}

void free_operation(Operation* op, uint8_t with_iter) {
//    if (op->intent == SI_UPDATE) {
//        free_row_content(op->iterator->table->mapped_addr->fields_n, op->update_row);
//    }
    if (with_iter) {
        RowsIterator* iter = op->iterator;
        while (iter) {
            RowsIterator* next = iter->outer;
            close_table(iter->storage, (OpenedTable*)iter->table);
            free((void*)iter->table);
            rows_iterator_free(iter);
            iter = next;
        }
    }

    free(op->table_alias);
    free(op->update_row);
    free(op);
}
//
// Created by vlad on 02.01.24.
//

#ifndef LAB3_INTERPRETER_H
#define LAB3_INTERPRETER_H

#include "../common/ast.h"
#include "ienv.h"
#include "database/util.h"
#include "database/rows_iterator.h"
#include "database/search_filters.h"

typedef enum Intent { SI_NOTHING, SI_REMOVE, SI_GET, SI_UPDATE, SI_INSERT, SI_CREATE } Intent;
typedef struct IEnv IEnv;

typedef struct Operation {
    RowsIterator* iterator;
    enum Intent intent;
    char* table_alias;
    struct Operation* parent;
    union {
        uint8_t* selection_mask;
        void** update_row;
    };
} Operation;

void free_operation(Operation* op, uint8_t with_iter);
IEnv* interpret_root(Ast* node, Storage* storage);

#endif //LAB3_INTERPRETER_H

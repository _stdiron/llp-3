#include <stdio.h>
#include <xmlrpc.h>
#include <xmlrpc_server.h>
#include <xmlrpc-c/server_abyss.h>
#include "../common/ast_xml_convert.h"
#include "database/linked_list.h"
#include "interpreter.h"
#include "database/storage_intlzr.h"

#define IENVS_N 10

Storage* storage;
IEnv* continuable[IENVS_N];

int save_ienv(IEnv* env) {
    for (int i = 0; i < IENVS_N; i++) {
        if (continuable[i] == 0) {
            continuable[i] = env;
            return i;
        }
    }
    return -1;
}

static xmlrpc_value* interpret_ast(
        xmlrpc_env*   const env,
        xmlrpc_value* const paramArray,
        void* const serverInfo,
        void* const channelInfo) {

    xmlrpc_value* xml_ast;
    /* Parse our argument array. */
    xmlrpc_decompose_value(env, paramArray, "(S)", &xml_ast);
    Ast* ast_root = build_ast_from_xmlrpc(xml_ast, env);
    printf("Got:\n");
    print_ast(stdout, ast_root, 0);

    IEnv* ienv = interpret_root(ast_root, storage);
    make_response(ienv, env);
    free_ast(ast_root);

    xmlrpc_value* response = ienv->response;
    if (!ienv->error) {
        xmlrpc_value* ind_xml;
        if (ienv->finished) {
            free_ienv(ienv, storage);
            ind_xml = xmlrpc_int_new(env, -1);
        } else {
            int ind = save_ienv(ienv);
            if (ind == -1) {
                // cant store ienv, so free it
                printf("cant save ienv because of lack of memory\n");
                free_ienv(ienv, storage);
            }
            ind_xml = xmlrpc_int_new(env, ind);
        }
        xmlrpc_array_append_item(env, response, ind_xml);
        xmlrpc_DECREF(ind_xml);
    }
    return response;
}

static xmlrpc_value* continue_operation(
        xmlrpc_env*   const env,
        xmlrpc_value* const paramArray,
        void* const serverInfo,
        void* const channelInfo) {

    int index;
    IEnv* ienv;
    xmlrpc_decompose_value(env, paramArray, "(i)", &index);
    if ((ienv = continuable[index]) == NULL) {
        return make_error_response_string("cant found operation with the same id", env);
    }

    make_response(ienv, env);
    xmlrpc_value* response = ienv->response;
    xmlrpc_value* ind_xml;
    if (ienv->finished) {
        free_ienv(ienv, storage);
        ind_xml = xmlrpc_int_new(env, -1);
        continuable[index] = NULL;
    } else {
        ind_xml = xmlrpc_int_new(env, index);
    }
    xmlrpc_array_append_item(env, response, ind_xml);
    xmlrpc_DECREF(ind_xml);
    return response;
}

static xmlrpc_value* destroy_operation(
        xmlrpc_env*   const env,
        xmlrpc_value* const paramArray,
        void* const serverInfo,
        void* const channelInfo) {
    int index;
    IEnv* ienv;
    xmlrpc_decompose_value(env, paramArray, "(i)", &index);
    if ((ienv = continuable[index]) == NULL)
        return xmlrpc_build_value(env, "(i)", RESPONSE_OK);
    free_ienv(ienv, storage);
    continuable[index] = NULL;
    return xmlrpc_build_value(env, "(i)", RESPONSE_OK);
}

int main(int args_n, char* args[]) {
    xmlrpc_server_abyss_parms server_params;
    xmlrpc_env env;
    if (args_n < 3) {
        fprintf(stderr, "You need to pass args: <path-to-db> <port>\n");
        exit(1);
    }
    memset(continuable, 0, sizeof(void*) * IENVS_N);

    xmlrpc_env_init(&env);
    storage = init_storage(args[1]);
    server_params.registryP = xmlrpc_registry_new(&env);
    xmlrpc_check_fault(&env);

    struct xmlrpc_method_info3 const interpret_method_info = {
            .methodName     = "interpret_ast",
            .methodFunction = &interpret_ast,
    };
    struct xmlrpc_method_info3 const continue_method_info = {
            .methodName     = "continue_operation",
            .methodFunction = &continue_operation,
    };
    struct xmlrpc_method_info3 const destroy_method_info = {
            .methodName     = "destroy_operation",
            .methodFunction = &destroy_operation,
    };
    xmlrpc_registry_add_method3(&env, server_params.registryP, &interpret_method_info);
    xmlrpc_registry_add_method3(&env, server_params.registryP, &continue_method_info);
    xmlrpc_registry_add_method3(&env, server_params.registryP, &destroy_method_info);
    xmlrpc_check_fault(&env);

    server_params.config_file_name = 0;
    server_params.port_number = atoi(args[2]);
    if (server_params.port_number == 0) {
        fprintf(stderr, "Cant parse port argument\n");
        exit(-1);
    }
    server_params.log_file_name    = "/home/vlad/Music/log";
    printf("[LOG] Server started\n");

    xmlrpc_server_abyss(&env, &server_params, XMLRPC_APSIZE(log_file_name));
    xmlrpc_check_fault(&env);

    return 0;
}


/*
 * start >
 * interp_root_operation() [FOR, INSERT, CREATE]
 *      for -> interp_operation() [FOR, RETURN, UPDATE, REMOVE, FILTER]
 *      insert -> interp_doc() [document]
 *      create -> interp_def() [definition]
 *
 * !we need to pass OpenedTable as an arg if possible!
 * interp_operation() [FOR, RETURN, UPDATE, REMOVE]
 *      for -> interp_operation() [FOR, RETURN, UPDATE, REMOVE] ret RowsIterator + intent (Remove/Return/Update)
 *      filter -> interp_conditions [condition] ret RequestFilters
 *      update -> interp_doc() ret intent
 *      remove ret intent
 *      return -> parse_merge()? ret intent + identifiers of data we want to return
 */

/*
 * Potential leafs of ast:
 *
 * FOR:
 * 1. RETURN
 * 2. MERGE
 * 3. UPDATE
 * 4. REMOVE
 *
 * OTHER:
 * 1. INSERT
 * 2. CREATE
 *
 */